# CSCI7772 Assignment 5 - Sampling - Geoffrey Sutcliffe

#### https://bitbucket.org/geoff_sutcliffe/sampling/

<hr>

##Part I
###a) 
CU, CS: 0.87563

CU, Business: 0.11773

metro, CS: 0.0045139

metro, Business: 0.0021212

###b)
CU, CS: 0.59071

CU, Business: 0.30183

metro, CS: 0.045055

metro, Business: 0.062398


###c)
CU, CS: 0.022836

CU, Business: 0.079358

metro, CS: 0.1017

metro, Business: 0.79611

###d)
One interesting observation is that for all instances of Salary, it is unlikely for a student to have attended the Metro CS program. In general terms, this is likely because of the tight factoring of the Bayes' net. Every variable is tied to Intelligence. A low value of intelligence tends to push a sample towards Metro, but it also pushes that sample towards Business.

##Part II
![](img/part2_x1.png)
![](img/part2_x2.png)

The initialization chosen was x1 = 0.5, x2 = 0.5.
The burn in duration ran for 1000 iterations - the end of the burn-in period was used as the starting point for sampling.
10000 samples were obtained from each chain - an iteration has been considered to be an update of both chains, as this makes sense for our system with only two variables.

##Part III
###a)

![](img/part3_JD.png)

###b)
Expected value = 0.47768

## Appendix A
### Code for Part I
	
	% Evidence variables - Salary
	% Joint posterior - University and Major
	% We sample from the unobserved variables (Intelligence, Major, University)
	for n = sample_range
    	% Initialize the weight to 1
    	W(n) = 1.0;
    	%intelligence
	    I(n) = int_mean + randn*int_sd; % Normal(mean=100, sd=15)
	    %major
	    M(n) = rand < 1/(1+exp(-(I(n)-100)/5)); % = 1 if Major=compsci, 0 if Major=business
	    %university
	    U(n) = rand < 1/(1+exp(-(I(n)-100)/5)); % = 1 if University=cu, 0 if University=metro
	    %salary
	    %S(n) = gamrnd(.1 * I(n) + M(n) + 3 * U(n),5); % draw a salary from Gamma density with scale=5
	    S(n) = salary;
	    % Weight should be equal to P(S=120|M=m,U=u,I=i)
	    W(n) = W(n) * gampdf(S(n), .1 * I(n) + M(n) + 3 * U(n),5);
	    
	    %Likelihood = P(M=m|I=i) * P(U=u|I=i)
	    %not sure this is correct...
	    L(n) = (1/(1+exp(-(I(n)-110)/5)) ) * (1/(1+exp(-(I(n)-100)/5)) );
    
		weighted_samples(n) = W(n) * L(n);

	    % Now store the weighted counts which need to be normalized later
	    major = M(n);
	    university = U(n);
	    if university == 1
	        if major == 1
	            counts(1, 1) = counts(1,1) + weighted_samples(n);
	        else
	            counts(1, 2) = counts(1, 2) + weighted_samples(n);
	        end
	    else
	        if major == 1
	            counts(2, 1) = counts(2, 1) + weighted_samples(n);
	        else
	            counts(2, 2) = counts(2, 2) + weighted_samples(n);
	        end
	    end
	    total_weight = total_weight + weighted_samples(n);
		end

	posteriors = zeros(2,2);
	%uni = CU, major = CS
	posteriors(1,1) = counts(1,1)/total_weight;
	%uni = CU, major = business
	posteriors(1,2) = counts(1,2)/total_weight;
	%uni = metro, major = CS
	posteriors(2,1) = counts(2,1)/total_weight;
	%uni = metro, major = business
	posteriors(2,2) = counts(2,2)/total_weight;
	
	disp(['CU, CS: ' num2str(posteriors(1,1))]);
	disp(['CU, Business: ' num2str(posteriors(1,2))]);
	disp(['metro, CS: ' num2str(posteriors(2,1))]);
	disp(['metro, Business: ' num2str(posteriors(2,2))]);

##Appendix B
###Code for Part II
	close all
	clear all
	set(0,'DefaultFigureWindowStyle','docked')

	N=10000;
	burn_in = 1000;
	burn_in_range = 1:burn_in;
	sample_range = 1:N;
	mean = [1; 0];
	inv_mean = flipud(mean);
	covariance = [1 -0.5; -0.5 2];
	inv_covariance = fliplr(flipud(covariance));
	samples = zeros(N,2);
	burn_in_set = zeros(burn_in,2);

	%init x1 chain, and x2 chain


	for n = burn_in_range
        % should we lock down x1 or x2
	    selection = randi(2);
	    if selection == 1
        	new_x1 = conditional(burn_in_set(n, 2), mean, covariance);
        	new_x2 = conditional(new_x1, inv_mean, inv_covariance);
        	burn_in_set(n+1,:) = [new_x1 new_x2];
    	else
	        new_x2 = conditional(burn_in_set(n, 1), inv_mean, inv_covariance);
        	new_x1 = conditional(new_x2, mean, covariance);
        	burn_in_set(n+1,:) = [new_x1 new_x2];
    	end
	end

	iterations(1,:) = burn_in_set(burn_in,:);

	for n = sample_range
	    % should we lock down x1 or x2
	    selection = randi(2);
	    if selection == 1
	        new_x1 = conditional(iterations(n, 2), mean, covariance);
	        new_x2 = conditional(new_x1, inv_mean, inv_covariance);
	        iterations(n+1,:) = [new_x1 new_x2];
	    else
	        new_x2 = conditional(iterations(n, 1), inv_mean, inv_covariance);
	        new_x1 = conditional(new_x2, mean, covariance);
	        iterations(n+1,:) = [new_x1 new_x2];
    	end
	end


	hist_x1 = histogram(iterations(:,1), 'Normalization','probability')
	hist_range = 2*hist_x1.BinLimits(1):hist_x1.BinWidth:2*hist_x1.BinLimits(2);
	scale = max(hist_x1.Values) / max(normpdf(hist_range, mean(1,1), sqrt(covariance(1,1) ) ) )
	hold on
	plot(hist_range,scale*normpdf(hist_range, mean(1,1), sqrt(covariance(1,1))))
	title('P(X1)');

	figure()
	hist_x2 = histogram(iterations(:,2), 'Normalization','probability');
	hold on
	hist_range = 2*hist_x2.BinLimits(1):hist_x2.BinWidth:2*hist_x2.BinLimits(2);
	scale = max(hist_x2.Values) / max(normpdf(hist_range, mean(2,1), sqrt(covariance(2,2) ) ) )
	plot(hist_range, scale*normpdf(hist_range, mean(2,1), sqrt(covariance(2,2))))
	title('P(X2)');

---

	function [ p ] = conditional( y_, mu_, Sigma_ )
	% example, mu = [1;0] y = float sigma = [1  , -0.5 ;
	%                              			-0.5,   2 ]
		inv_Sigma = pinv(Sigma_);
		mean = mu_(1,1)+ (Sigma_(1,2) * inv_Sigma(2,2) * (y_-mu_(2,1)));
		std_dev = Sigma_(1,1) - ( Sigma_(1,2) * inv_Sigma(2,2)) * Sigma_(2,1);
		
		%p = mean + std_dev.*randn;
		p = normrnd(mean,std_dev);
	end

##Appendix C
###Code for Part III
	function result = metropolis_hastings  
	    clear all
	    close all
	
	    N = 10000;
	
	    s = 0.01;
	    prop_pdf = @(x, y) prod(unifpdf(y, y-s, y+s));
	    prop_rnd = @(x) rand(1,2);
	
	    [smpl, accept] = mhsample([0.5 0.5], N, 'pdf',  @pdf, 'proppdf', prop_pdf, 'proprnd', prop_rnd, 'burnin', 1000);   
	
	    % Visualization from Mike
	    binEdges = 0:.05:1;
	    chart = hist3(smpl, 'Edges', {binEdges binEdges});
	    pcolor(binEdges, binEdges, chart);
	    ylabel('F'); xlabel('G'); axis square
	    %colorbar % no good cos its not normalized
	
	    disp(['Accept = ' , num2str(accept*100), '%']);
	    % The mean of the samples f*g will produce the expected value of the
	    % distribution - E(f,g)
	    disp(['Expected value = ' , num2str(sum(prod(smpl,2))/length(smpl))]);
	    %figure();
	    %histogram(prod(smpl,2));
	    %title('E(f,g)');
	end

---

	function p = pdf(X)
	    f = X(1,1);
	    g = X(1,2);
	    
	    p_f = 0;
	    p_g_f = 0;
	    
	    if (f >= 0 && f <= 1)
	        p_f = f^3;
	    end
	    
	    if (g >= 0 && g <= 1)
	        p_g_f = 1 - abs(g-f);
	    end
	
	    p = p_f * p_g_f;
    
	end