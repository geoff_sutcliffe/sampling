function [ p ] = conditional( y_, mu_, Sigma_ )
%CONDITIONAL Summary of this function goes here
%   Detailed explanation goes here
% mu = [1;0] y = float sigma = [1  , -0.5 ;
%                              -0.5,   2 ]
inv_Sigma = pinv(Sigma_);
mean = mu_(1,1)+ (Sigma_(1,2) * inv_Sigma(2,2) * (y_-mu_(2,1)));
std_dev = Sigma_(1,1) - ( Sigma_(1,2) * inv_Sigma(2,2)) * Sigma_(2,1);

%p = mean + std_dev.*randn;
p = normrnd(mean,std_dev);
end

